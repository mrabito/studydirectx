﻿#include "stdafx.h"
#include "D3D.h"

int DrawFigure(float x, float y, float width, float height, unsigned long color)
{
	g_pD3DDev->SetTexture (0, nullptr);	

	Vertex v[4]; //ポリゴンの頂点を表す構造体
	const float wh = width / 2;
	const float hh = height / 2;

	// 描画する図形の4つの点の座標
	v[0].pos = D3DXVECTOR3(x - wh, y - hh, 0);
	v[1].pos = D3DXVECTOR3(x + wh, y - hh, 0);
	v[2].pos = D3DXVECTOR3(x - wh, y + hh, 0);
	v[3].pos = D3DXVECTOR3(x + wh, y + hh, 0);

	// テクスチャ画像上の座標
	v[0].texPos = D3DXVECTOR2(0, 0);
	v[1].texPos = D3DXVECTOR2(1, 0);
	v[2].texPos = D3DXVECTOR2(0, 1);
	v[3].texPos = D3DXVECTOR2(1, 1);

	for (int i = 0; i < 4; ++i)
	{
		v[i].diffuse = color;
		v[i].rhw = 1.0f;	//必要
	}

	// デバイスに描画開始を通知
	if (FAILED(g_pD3DDev->BeginScene()))
		return -1;

	// 描画
	g_pD3DDev->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, v, sizeof(Vertex));
	// TRIANGLESTRIPで描画、２つのポリゴンで描画(使えるポリゴンを制限する的な)、 頂点情報、メモリのサイズ

	// 描画終了を通知
	g_pD3DDev->EndScene();
	return 0;
}