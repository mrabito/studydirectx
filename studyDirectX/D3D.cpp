﻿//=============================================================================
//
// Direct3D描画関係処理 [D3D.cpp]
//
//=============================================================================
#include "stdafx.h"
#include "D3D.h"

//*****************************************************************************
// グローバル変数
//*****************************************************************************
LPDIRECT3D9 g_pD3D = nullptr;
LPDIRECT3DDEVICE9 g_pD3DDev = nullptr;

//=============================================================================
// Direct3Dの初期化
//=============================================================================
int InitializeD3D(HWND hWnd, bool fullScreen, UINT width, UINT height)
{
	D3DDISPLAYMODE dispMode; // ディスプレイモード
	D3DPRESENT_PARAMETERS presentParam; // プレゼントパラメータ

	try
	{
		// Direct3D9インターフェースの生成
		g_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
		if (g_pD3D == nullptr)
			throw 0;

		// ディスプレイモードの取得
		if (FAILED(g_pD3D->GetAdapterDisplayMode(0, &dispMode)))
			throw 1;

		// プレゼントパラメータの取得
		ZeroMemory(&presentParam, sizeof(presentParam));
		presentParam.BackBufferFormat = dispMode.Format; // ビット深度
		presentParam.BackBufferWidth = width; // バックバッファの幅
		presentParam.BackBufferHeight = height; // バックバッファの高さ
		presentParam.BackBufferCount = 1; // バックバッファの数
		presentParam.SwapEffect = D3DSWAPEFFECT_FLIP; // スワップエフェクト
		presentParam.Flags = D3DPRESENTFLAG_LOCKABLE_BACKBUFFER; // バックバッファをロック可能にする
		presentParam.Windowed = fullScreen ? FALSE : TRUE; // 表示モード

														   // Zバッファの自動生成
		presentParam.EnableAutoDepthStencil = TRUE;
		presentParam.AutoDepthStencilFormat = D3DFMT_D24S8; // ビット震度16
		presentParam.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
		presentParam.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;	// V-Syncを待たない

																			// デバイスの生成
		if (FAILED(g_pD3D->CreateDevice(0, D3DDEVTYPE_HAL, hWnd,
			D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
			&presentParam, &g_pD3DDev)))
		{
			if (FAILED(g_pD3D->CreateDevice(0, D3DDEVTYPE_HAL, hWnd,
				D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
				&presentParam, &g_pD3DDev)))
			{
				if (FAILED(g_pD3D->CreateDevice(0, D3DDEVTYPE_REF, hWnd,
					D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
					&presentParam, &g_pD3DDev)))
					throw 2;
			}
		}

		// αブレンドを有効
		g_pD3DDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
		g_pD3DDev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		g_pD3DDev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

		// テクスチャステージステート
		g_pD3DDev->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
		g_pD3DDev->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
		g_pD3DDev->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);

		g_pD3DDev->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
		g_pD3DDev->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
		g_pD3DDev->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);

		// Ｚバッファ有効
		g_pD3DDev->SetRenderState(D3DRS_ZENABLE, TRUE);

		// 頂点フォーマットの設定
		g_pD3DDev->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1);
	}
	catch (int error)
	{
		switch (error)
		{
		case 0:
			MessageBox(hWnd, TEXT("ディスプレイの取得に失敗しました。"), TEXT("アナ"), MB_ICONERROR);
			break;
		case 1:
			MessageBox(hWnd, TEXT("ディスプレイモードの取得に失敗しました。"), TEXT("D3Dエラー"), MB_ICONERROR);
			break;
		case 2:
			MessageBox(hWnd, TEXT("D3Dデバイスの生成に失敗しました。"), TEXT("D3Dエラー"), MB_ICONERROR);
			break;
		default:
			MessageBox(hWnd, TEXT("未定義のエラーが発生しました。"), TEXT("D3Dエラー"), MB_ICONERROR);
			break;
		}

		return -1;
	}

	return 0;
}

//=============================================================================
// Direct3Dの終了処理
//=============================================================================
void FinalizeD3D()
{
	// D3Dデバイスオブジェクトの解放
	if (g_pD3DDev != nullptr)
	{
		g_pD3DDev->Release();
		g_pD3DDev = nullptr;
	}
	// D3Dオブジェクトの解放
	if (g_pD3D != nullptr)
	{
		g_pD3D->Release();
		g_pD3D = nullptr;
	}
}

//=============================================================================
// 画面のクリア
//=============================================================================
void ClearScreen()
{
	g_pD3DDev->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(0, 0, 0), 1.0f, 0);
}

//=============================================================================
// 画面のフリップ
//=============================================================================
void FlipScreen()
{
	g_pD3DDev->Present(NULL, NULL, NULL, NULL);
}
