﻿#pragma once
#include <d3d9.h>
#include <d3dx9.h>

#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")

//*****************************************************************************
// 構造体
//*****************************************************************************
// ポリゴンの頂点データ
struct Vertex
{
	D3DXVECTOR3 pos;
	float rhw;
	D3DCOLOR diffuse;
	D3DXVECTOR2 texPos;
};


//******************************************************
// プロトタイプ宣言
//******************************************************

int InitializeD3D (HWND hWnd, bool fulScreen, UINT screenWidth, UINT screenHeight);
//終了処理
void FinalizeD3D ();

void ClearScreen ();
//スクリーンの裏面に予め描画しておいて、実際に表示させる時はそれを裏返して表面にする
void FlipScreen ();	


//*******************************************************
// EXTERN宣言
//*******************************************************
// 変数を色んなcppで共有する。変数のプロトタイプ宣言みたいな
extern LPDIRECT3DDEVICE9 g_pD3DDev;
